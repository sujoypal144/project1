function GameForKids() {
    var quesNumber;
    var arr = [8];

    function game() {
        let questions = ["Click on circle", "Click on rectangle", "Click on triangle",
            "Click on square", "Click on cloud", "Click on flower", "Click on tree", "Click on crow"];
        quesNumber = Math.floor(Math.random() * questions.length);
        alert(questions[quesNumber]);
    }

    function next() {
        let confirmation = confirm("Wants to play!!!");
        if (confirmation) {
            game();
        }
        else {
            $('.play-area').html("Thanks for Playing.");
        }
    }
    function retry() {
        alert("Try Again");
    }
    
    function matchCount(arr, element) {
        var count = 0;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] === element) {
                count++;
            }
        }
        return count;
    }
    $(document).ready(function () {

        $('.circle').on('click', function () {
            if (quesNumber === 0) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.rectangle').on('click', function () {
            if (quesNumber === 1) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.triangle').on('click', function () {
            if (quesNumber === 2) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.square').on('click', function () {
            if (quesNumber === 3) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.cloud').on('click', function () {
            if (quesNumber === 4) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.flower').on('click', function () {
            if (quesNumber === 5) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.tree').on('click', function () {
            if (quesNumber === 6) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });
        $('.crow').on('click', function () {
            if (quesNumber === 7) {
                alert("Congratulations!!! Correct image was clicked.");
                next();
            }
            else {
                retry();
            }
        });

    });
    return {
        game: game
    };
}
var gameObj = GameForKids();
gameObj.game();