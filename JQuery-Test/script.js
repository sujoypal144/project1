function SubmitForm() {
    function validations() {
        $(function () {
            $("#fname_error_message").hide();
            $("#lname_error_message").hide();
            $("#email_error_message").hide();
            $("#cNumber_error_message").hide();
            $("#make_error_message").hide();
            $("#model_error_message").hide();
            $("#year_error_message").hide();
            $("#color_error_message").hide();
            $("#problems_error_message").hide();
            $(".car-list").hide();

            var error_fname = false;
            var error_lname = false;
            var error_email = false;
            var error_cNumber = false;
            var error_make = false;
            var error_model = false;
            var error_year = false;
            var error_color = false;
            var error_problems = false;

            $("#firstName").focusout(function () {
                check_fname();
            });
            $("#lastName").focusout(function () {
                check_lname();
            });
            $("#email").focusout(function () {
                check_email();
            });
            $("#contactNumber").focusout(function () {
                check_cNumber();
            });
            $("#make").focusout(function () {
                check_make();
            });
            $("#model").focusout(function () {
                check_model();
            });
            $("#year").focusout(function () {
                check_year();
            });
            $("#color").focusout(function () {
                check_color();
            });
            $("#problems").focusout(function () {
                check_problems();
            });

            function check_fname() {
                var reg = /^[a-zA-Z]*$/;
                var fname = $("#firstName").val();
                if (reg.test(fname) && fname !== '') {
                    $("#fname_error_message").hide();
                }
                else {
                    $("#fname_error_message").html("Required and Should contain only characters");
                    $("#fname_error_message").show();
                    error_fname = true;
                }
            }
            function check_lname() {
                var reg = /^[a-zA-Z]*$/;
                var lname = $("#lastName").val();
                if (reg.test(lname) && lname !== '') {
                    $("#lname_error_message").hide();
                }
                else {
                    $("#lname_error_message").html("Required and Should contain only characters");
                    $("#lname_error_message").show();
                    error_lname = true;
                }
            }
            function check_email() {
                var reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                var email = $("#email").val();
                if (reg.test(email) && email !== '') {
                    $("#email_error_message").hide();
                }
                else {
                    $("#email_error_message").html("Required and Must be valid");
                    $("#email_error_message").show();
                    error_email = true;
                }
            }
            function check_cNumber() {
                var reg = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
                var cNumber = $("#contactNumber").val();
                if (reg.test(cNumber) && cNumber !== '') {
                    $("#cNumber_error_message").hide();
                }
                else {
                    $("#cNumber_error_message").html("Required and Must be valid");
                    $("#cNumber_error_message").show();
                    error_cNumber = true;
                }
            }
            function check_make() {
                var make = $("#make").val();
                if (make !== '') {
                    $("#make_error_message").hide();
                }
                else {
                    $("#make_error_message").html("Required and Must be valid");
                    $("#make_error_message").show();
                    error_make = true;
                }
            }
            function check_model() {
                var model = $("#model").val();
                var make = $("#make").val();

                if (model !== '') {
                    $("#model_error_message").hide();

                }
                else {
                    $("#model_error_message").html("Required and Must be valid");
                    $("#model_error_message").show();
                    error_model = true;
                }
            }
            function check_year() {
                var year = $("#year").val();
                if (year !== '') {
                    $("#year_error_message").hide();
                }
                else {
                    $("#year_error_message").html("Required and Must be valid");
                    $("#year_error_message").show();
                    error_year = true;
                }
            }
            function check_color() {
                var color = $("#color").val();
                if (color !== '') {
                    $("#color_error_message").hide();
                }
                else {
                    $("#color_error_message").html("Required and Must be valid");
                    $("#color_error_message").show();
                    error_color = true;
                }
            }
            function check_problems() {
                var problems = $("#problems").val();
                if (problems !== '') {
                    $("#problems_error_message").hide();
                }
                else {
                    $("#problems_error_message").html("Required and Must be valid");
                    $("#problems_error_message").show();
                    error_problems = true;
                }
            }
            $('.submit-btn').click(function () {
                error_fname = false;
                error_lname = false;
                error_email = false;
                error_cNumber = false;
                error_make = false;
                error_model = false;
                error_year = false;
                error_color = false;
                error_problems = false;

                check_fname();
                check_lname();
                check_email();
                check_cNumber();
                check_make();
                check_model();
                check_year();
                check_color();
                check_problems();

                if (error_fname === false &&
                    error_lname === false &&
                    error_email === false &&
                    error_cNumber === false &&
                    error_make === false &&
                    error_model === false &&
                    error_year === false &&
                    error_color === false &&
                    error_problems === false) {
                    $(".submit-text").html("You're car has been added to our garage for servicing.").css('background-color', 'green');
                    $(".submit-text").fadeOut(5000);
                $('.entries').html(`<tr><td>${$("#firstName").val()}</td><td>${$("#lastName").val()}</td>
                <td>${$("#email").val()}</td><td>${$("#contactNumber").val()}</td>
                <td>${$("#make").val()}</td><td>${$("#model").val()}</td>
                <td>${$("#year").val()}</td><td>${$("#color").val()}</td>
                <td>${$("#problems").val()}</td><td><input type="checkbox"/></td></tr>`);
                    $("tr th").css('border', '1px #000000 solid');
                    $(".car-list").show();
                    return false;
                }
                else {
                    $(".submit-text").html("Please fill the form corectly").css('background-color', 'red');
                    $(".submit-text").fadeOut(5000);
                    $("tr th").css('border', '1px #000000 solid');
                    $(".car-list").show();
                    return false;
                }

            });

        });
    }
    return {
        validations: validations
    };
}
$(document).ready(function () {
    var obj = SubmitForm();
    obj.validations();
});