function main() {
    var company = {
        "TCS": {
            "revenue": 1000000,
            "expenses": {
                "salary": 30,
                "rent": 20,
                "utilities": 15
            },
            "employees": [
                {
                    "name": "Joe",
                    "age": 30,
                    "role": "Admin"
                },
                {
                    "name": "Henry",
                    "age": 40,
                    "role": "Tester"
                },
                {
                    "name": "Sherlock",
                    "age": 45,
                    "role": "Programmer"
                }
            ]

        },
        "GGK": {
            "revenue": 500000,
            "expenses": {
                "salary": 35,
                "rent": 15,
                "utilities": 10
            },
            "employees": [
                {
                    "name": "Josh",
                    "age": 32,
                    "role": "Admin"
                },
                {
                    "name": "Mark",
                    "age": 35,
                    "role": "Tester"
                },
                {
                    "name": "Tony",
                    "age": 50,
                    "role": "Programmer"
                }
            ]

        },
        "Osmosys": {
            "revenue": 800000,
            "expenses": {
                "salary": 28,
                "rent": 25,
                "utilities": 10
            },
            "employees": [
                {
                    "name": "Antony",
                    "age": 28,
                    "role": "Admin"
                },
                {
                    "name": "David",
                    "age": 43,
                    "role": "Tester"
                },
                {
                    "name": "Racheal",
                    "age": 37,
                    "role": "Programmer"
                }
            ]
        }
    }
    employeeAge(company);
    roles(company);
    companyAtProfit(company);
}


function employeeAge(company) {
    var thresAge = prompt("Enter the threshold age:");
    console.log(`The employees having age less than ${thresAge} are:`);
    for (var [key, value] of Object.entries(company)) {
        for (var i = 0; i < value.employees.length; i++) {
            var employeeAge = value.employees[i].age;
            if (employeeAge <= thresAge) {
                console.log(value.employees[i].name);
            }
        }
    }
    console.log(`The employees having age greater than ${thresAge} are:`);
    for (var [key, value] of Object.entries(company)) {
        for (var i = 0; i < value.employees.length; i++) {
            var employeeAge = value.employees[i].age;
            if (employeeAge > thresAge) {
                console.log(value.employees[i].name);
            }
        }
    }
    var maxAge = 0;
    var minAge = 60;
    var oldest;
    var youngest;
    for (var [key, value] of Object.entries(company)) {
        if (key == "TCS") {
            for (var i = 0; i < value.employees.length; i++) {
                if (value.employees[i].age >= maxAge) {
                    maxAge = value.employees[i].age;
                    oldest = value.employees[i].name
                }
                if (value.employees[i].age <= minAge) {
                    minAge = value.employees[i].age;
                    youngest = value.employees[i].name;
                }
            }
        }
    }
    console.log(`The oldest employee in TCS is: ${oldest}`);
    console.log(`The youngest employee in TCS is: ${youngest}`);

    var maxAge = 0;
    var minAge = 60;
    var oldest;
    var youngest;

    for (var [key, value] of Object.entries(company)) {
        if (key == "GGK") {
            for (var i = 0; i < value.employees.length; i++) {
                if (value.employees[i].age >= maxAge) {
                    maxAge = value.employees[i].age;
                    oldest = value.employees[i].name
                }
                if (value.employees[i].age <= minAge) {
                    minAge = value.employees[i].age;
                    youngest = value.employees[i].name;
                }
            }
        }
    }
    console.log(`The oldest employee in GGK is: ${oldest}`);
    console.log(`The youngest employee in GGK is: ${youngest}`);

    var maxAge = 0;
    var minAge = 60;
    var oldest;
    var youngest;

    for (var [key, value] of Object.entries(company)) {
        if (key == "Osmosys") {
            for (var i = 0; i < value.employees.length; i++) {
                if (value.employees[i].age >= maxAge) {
                    maxAge = value.employees[i].age;
                    oldest = value.employees[i].name
                }
                if (value.employees[i].age <= minAge) {
                    minAge = value.employees[i].age;
                    youngest = value.employees[i].name;
                }
            }
        }
    }
    console.log(`The oldest employee in Osmosys is: ${oldest}`);
    console.log(`The youngest employee in Osmosys is: ${youngest}`);
}


function roles(company) {
    console.log("Names of the Admins in different companies:")
    for (var [key, value] of Object.entries(company)) {
        for (var i = 0; i < value.employees.length; i++) {
            if (value.employees[i].role === "Admin") {
                console.log(value.employees[i].name);
            }
        }
    }
    console.log("Names of the Testers in different companies:")
    for (var [key, value] of Object.entries(company)) {
        for (var i = 0; i < value.employees.length; i++) {
            if (value.employees[i].role === "Tester") {
                console.log(value.employees[i].name);
            }
        }
    }
    console.log("Names of the Programmers in different companies:")
    for (var [key, value] of Object.entries(company)) {
        for (var i = 0; i < value.employees.length; i++) {
            if (value.employees[i].role === "Programmer") {
                console.log(value.employees[i].name);
            }
        }
    }

}


function companyAtProfit(company) {
    var profit = [];
    var i = 0;
    var max = 0;
    var c;
    console.log("Profits of the companies:");
    for (var [key, value] of Object.entries(company)) {
        var sal = (value.expenses.salary) * (value.revenue) / 100;
        var r = (value.expenses.rent) * (value.revenue) / 100;
        var uti = (value.expenses.utilities) * (value.revenue) / 100;
        profit[i] = (value.revenue) - (sal + r + uti);
        console.log(`${key}: ${profit[i]}`);
        if (profit[i] >= max) {
            max = profit[i];
            c = key;
        }
        i++;

    }
    console.log(`The company that makes maximum profit is: ${c} and the profit amount is:${max}`);
}

main();
