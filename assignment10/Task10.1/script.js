function charFreq(str){
    const temp={};
    for(var i=0;i<str.length;i++)
    {
       const c =str[i];
       if(!temp[c]){
           temp[c]=0;
       }
       temp[c]++;
    }
    return temp;
}
function main(){
    var str=prompt("Enter the string:");
    console.log(charFreq(str));
}
main();