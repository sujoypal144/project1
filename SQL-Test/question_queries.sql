use res_productions;
INSERT INTO `res_productions`.`author_details` (`id`, `Author name`, `Date of birth`, `Description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`) 
VALUES ('1', 'J.K. Rowling', '1964-08-22', 'writer of harry potter series', '2018-08-15', 'RES', '2018-08-15', 'RES', '1');
INSERT INTO `res_productions`.`publisher_details` (`id`, `Publisher name`, `Established on`, `Address`, `Description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`) 
VALUES ('1', 'Pengiun', '2001-02-25', 'xxxx', 'famous publisher', '2018-08-25', 'RES', '2018-08-25', 'RES', '1');
INSERT INTO `res_productions`.`publisher_details` (`id`, `Publisher name`, `Established on`, `Address`, `Description`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`)
 VALUES ('2', 'Genius', '2005-04-12', 'xxxx', 'another famous publisher', '2019-03-07', 'RES', '2019-03-07', 'RES', '1');
SELECT * FROM `res_productions`.`publisher_details`;
INSERT INTO `res_productions`.`book_details` (`id`,`Book genre`) VALUES ('1','Suspense');
INSERT INTO `res_productions`.`book_details` (`id`,`Book genre`) VALUES ('2','Comedy');
INSERT INTO `res_productions`.`book_details` (`id`,`Book genre`) VALUES ('3','Romantic');
INSERT INTO `res_productions`.`user_registration` (`id`, `First name`, `Last name`, `Gender`, `Date of Birth`, `Normal/Premium user`, `Address`, `Phone Number`, `created_on`, `modified_on`, `created_by`, `modified_by`, `status`)
 VALUES ('1', 'Akash', 'Sarkar', 'M', '1997-03-22', 'Normal', 'xxxx', '9222222222', '2017-05-24', '2017-05-25', 'RES', 'RES', '1');
SELECT * FROM `res_productions`.`user_registration` WHERE `user_registration`.`Normal/Premium user` = 'Normal';
SELECT * FROM `res_productions`.`user_registration` WHERE `user_registration`.`Gender` = 'F';
SELECT 'Book genre' FROM `res_productions`.`book_details`;
SELECT 'Book title','Rating' FROM `res_productions`.`book_details` WHERE 'Rating'>4;
SELECT 'Book title',MAX('Rating') FROM `res_productions`.`book_details`;
SELECT 'Book title',MIN('Rating') FROM `res_productions`.`book_details`;
SELECT 'Author name' FROM `res_productions`.`author_details` WHERE 'Author name' LIKE 'AR%';
SELECT * FROM `res_productions`.`publisher_details` WHERE 'Established on' < 2012-01-01;
SELECT * FROM `res_productions`.`book_details` GROUP BY 'Rating' ORDER BY 'Rating' desc LIMIT 5;
SELECT * FROM `res_productions`.`book_details` WHERE 'Year of release'>='2012' AND 'Year of release'<='2018';
SELECT 'Book genre' FROM `res_productions`.`book_details` GROUP BY 'Book genre' ORDER BY COUNT('Book genre') desc LIMIT 1;
SELECT 'Author name' FROM `res_productions`.`author_details`;
SELECT 'Publisher name' FROM `res_productions`.`publisher_details` JOIN `res_productions`.`book_details` ON `res_productions`.`publisher_details`.`id`=`res_productions`.`book_details`.`id`; 
SELECT 'First Name','LastName' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id` WHERE 'rent_date'= true;
SELECT 'First Name','LastName' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id` WHERE 'like_date'= true;
SELECT 'First Name','LastName' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id` WHERE 'friend_id'= true;
SELECT 'First Name','LastName' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id` WHERE 'wishlist_date'= true;

SELECT 'First Name' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id`
WHERE 'friend_id'=1;

SELECT 'First Name' ,'Last Name' FROM `res_productions`.`user_registration` JOIN 
`res_productions`.`features` ON `res_productions`.`user_registration`.`id`=`res_productions`.`features`.`user_id`
WHERE 'Normal/Premium user' = 'Premium' AND 'friend_id'=1;

SELECT 'Book title' FROM `res_productions`.`book_details` JOIN 
`res_productions`.`features` ON `res_productions`.`book_details`.`id`=`res_productions`.`features`.`user_id`
WHERE 'friend_id'=1 ANd 'like_date'=true;
