function Container() {
    function storage() {
        var files = [
            {
                "fileName": "flower",
                "type": "jpg"
            },
            {
                "fileName": "family-video-clip",
                "type": "mp4"
            },
            {
                "fileName": "phone-ringtone",
                "type": "mp3"
            },
            "javascript-excercises.txt",
            "learning-html-basics.rtf",
            {
                "fileName": "friend-video-clip",
                "type": "mp4"
            },
            {
                "fileName": "resume",
                "type": "docx"
            },
            {
                "fileName": "student-report",
                "type": "csv"
            },
            {
                "fileName": "sms-ringtone",
                "type": "mp3"
            },
            "html-basics.pdf",
            "dubsmash.mp4",
            "screen-shot.png",
            {
                "fileName": "faculty-report",
                "type": "xlsx"
            },
            {
                "fileName": "puppy",
                "type": "svg"
            }
        ]
        return files;
    }
    function operation() {
        var files=storage();
        var audio = [];
        var video = [];
        var document = [];
        var image = [];
        for (var i = 0; i < files.length; i++) {
            if (files[i].type === "mp3") {
                audio.push(files[i].fileName);
            }
            if (files[i].type === "mp4") {
                video.push(files[i].fileName);
            }

            if (files[i].type === "docx" || files[i].type === "csv" || files[i].type === "xlsx") {
                document.push(files[i].fileName);
            }

            if (files[i].type === "jpg" || files[i].type === "svg") {
                image.push(files[i].fileName);
            }

            var regDoc1 = /^[A-Za-z0-9-_,\s]+[.]{1}[txt]{3}$/;
            var regDoc2 = /^[A-Za-z0-9-_,\s]+[.]{1}[rtf]{3}$/;
            var regDoc3 = /^[A-Za-z0-9-_,\s]+[.]{1}[pdf]{3}$/;
            if (regDoc1.test(files[i]) || regDoc2.test(files[i]) || regDoc3.test(files[i])) {
                document.push(files[i]);
            }
            var regImg = /^[A-Za-z0-9-_,\s]+[.]{1}[png]{3}$/;
            if (regImg.test(files[i])) {
                image.push(files[i]);
            }
            var regVideo = /^[A-Za-z0-9-_,\s]+[.]{1}[mp4]{3}$/;
            if (regVideo.test(files[i])) {
                video.push(files[i]);
            }

        }

        var sortedFiles = {
            "audio": audio,
            "video": video,
            "document": document,
            "image": image
        };
        console.log(sortedFiles);

    }

    return {
        operation: operation
    };
}
let obj = Container();
obj.operation();
