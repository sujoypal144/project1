function validation(inputDate) {
    var dateFormat = /^(0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])[\/\-]\d{4}$/;
    if (dateFormat.test(inputDate)) {
        return 1;
    }
    else {
        return 0;
    }
}
function countLeapYear(birthYear, currentYear) {
    var count = 0;
    for (var i = birthYear; i < currentYear; i++) {
        if (i % 4 == 0) {
            count++;
        }
        if (i % 400 == 0) {
            count++;
        }
    }
    return count;
}

function main(date) {
    
    if (validation(date) === 1) {
        console.log("Valid input");
        var a = date.split("-");
        var arr2 = [];
        for (var i = 0; i < a.length; i++) {
            arr2[i] = Number(a[i]);
        }
        var currentYear = "06-15-2020";
        var cyear = currentYear.split("-");
        var arr3 = [];
        for (var i = 0; i < cyear.length; i++) {
            arr3[i] = Number(cyear[i]);
        }
        var diff = arr3[2] - arr2[2];
        var months = 0;
        if (arr2[0] > arr3[0]) {
            months = ((diff - 1) * 12) + arr3[0];
        }
        else {
            var d = arr3[0] - arr2[0];
            months = (diff * 12) + d;
        }
        console.log(`You lived for ${months} months`);
        var weeks = months * 4;
        console.log(`You lived for ${weeks} weeks`);
        var count = countLeapYear(arr2[2], arr3[3]);
        var days = (diff - 1) * 365 + count + ((arr3[0] - 1) * 30) + arr3[1];
        console.log(`You lived for ${days} days`);
        var hrs = days * 24;
        console.log(`You lived for ${hrs}  hrs`);
        var secs = (hrs * 60 * 60);
        console.log(`You lived for ${secs} secs`);
    }
    else {
        console.log("invalid input");
    }
    
}
var date = prompt("enter the birth day in the format mm-dd-yyyy:");
console.log(setInterval(function(){
    main(date);
}, 2000));
//main(date);

