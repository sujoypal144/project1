var str1, str2, str3, str;
str1 = prompt("Enter the word or sentence:");
str2 = prompt("Enter the letter which is to be replaced:");
str3 = prompt("Enter the letter which will be placed in the place of the replaced letter:");
function escapeRegExp(string)
{
    return string.replace(/[.*+?^${}()|[\]\\]/g,"\\$&");
}
function replaceAll(str,term,replacement){
    return str.replace(new RegExp(escapeRegExp(term),'g'),replacement);
}
str=replaceAll(str1,str2,str3);
console.log(str);