var col, row, i, j, k;
col = prompt("Enter the column number:");
if (col % 2 != 0) {
    row = (col + 1)/2;
    for (i = 1; i <= row; i++) 
    {
        for (j = 1; j <= row - i; j++)
        {
            document.write("&nbsp&nbsp");
        }
        for (k = 1; k <= i + (i - 1); k++) 
        {
            document.write(k);
        }
        document.write("<br/>");
    }
}
else {
    document.write("You can only enter odd integer");
}
