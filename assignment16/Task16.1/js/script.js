function CircleGame() {
    var score = 0;
    var timeLimit = 60;
    var timerInterval;
    var circleGenerateInterval;
    var scoreConfigObj = ScoreConfig().scoreData;

    $('.play-area').on('click', 'div', function (event) {
        let className = event.target.className;
        let scoreGained = getScoreByClassName(className);
        score += scoreGained;
        $('.score-val').text(score);
    });


    function startGame() {

        // Timer is started
        $('.score-val').text(score);
        timerInterval = setInterval(function () {
            timeLimit = timeLimit - 1;
            $('.timer').text(timeLimit);
            if (timeLimit === 0) {
                endGame();
            }
        }, 1000);
        // - Start an setInterval which will count the time down by 1 sec.
        // Circle game also starts
        // - Start an setInterval which will generate random circles
        circleGenerateInterval = setInterval(function () {
            var circlePosition = generateRandomPosition();
            var circleObj = generateRandomCircle();
            $('.play-area').html(`<div class="${circleObj.className}"></div>`);
            $('.' + circleObj.className).css({
                position: 'absolute',
                left: circlePosition.x + 'px',
                top: circlePosition.y + 'px'
            });
        }, 2000);
    }

    function endGame() {
        // Responsible for ending the game.
        // Clear all the setIntervals
        clearInterval(timerInterval);
        clearInterval(circleGenerateInterval);
        $('h1').text('Game Over!!!');
        $('.play-area').html('');
    }

    function generateRandomPosition() {
        // Generate x, y coordinates for placing the circle
        var x = Math.floor((Math.random() * 750) + 1);
        var y = Math.floor((Math.random() * 550) + 1);
        return {
            x: x,
            y: y
        }
    }

    function generateRandomCircle() {
        // Give a random class name (red-circle, blue-circle)
        var scoreConfigLen = scoreConfigObj.length;
        var randomIndex = Math.floor((Math.random() * scoreConfigLen) + 0);
        return scoreConfigObj[randomIndex];
    }

    function calculateScore() {
        // Calculate score on clicking the circle
    }

    function getScoreByClassName(className) {
        let scoreArr = scoreConfigObj.filter(function (ele) {
            if (ele.className === className) {
                return true;
            }
            return false;
        });
        if (scoreArr.length > 0) {
            return scoreArr[0].score;
        }
        return 0;
    }

    return {
        startGame: startGame
    }
}

// Initialize the circle game
$(document).ready(function () {
    var circleGameInstance = CircleGame();
    circleGameInstance.startGame();
});